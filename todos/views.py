from django.shortcuts import render
from todos.models import TodoList

# Create your views here.


def list_todos(request):
    todolists = TodoList.objects.all()
    # template_name = f"/todos.html"
    context = {"todos": todolists}
    return render(request, "todos/main.html", context)


def detail_todos(request, pk):
    todolist = TodoList.objects.filter(pk=pk)
    context = {
        "detail": todolist,
    }
    return render(request, "todos/detail.html", context)
